<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>EmailList</name>
    <message>
        <location filename="../assets/EmailList.qml" line="7"/>
        <source>Sort by Sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/EmailList.qml" line="29"/>
        <source>Emails</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmailReader</name>
    <message>
        <location filename="../assets/EmailReader.qml" line="7"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/EmailReader.qml" line="12"/>
        <source>Back up!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/EmailReader.qml" line="26"/>
        <source>Save as..</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="141"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="153"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="159"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="167"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="173"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="181"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="187"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="194"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="199"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="209"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../assets/SettingsPage.qml" line="24"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="48"/>
        <source>Sort by Sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="75"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="109"/>
        <source>Set Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="128"/>
        <source>Save to HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="155"/>
        <source>Changes take effect after restarting the app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="12"/>
        <source>Contact us!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="27"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="33"/>
        <source>Rate Us!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="109"/>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="151"/>
        <source>Loading emails...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="167"/>
        <source>Email Accounts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
