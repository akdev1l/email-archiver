<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="109"/>
        <source>Accounts</source>
        <translation>Accounts</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="151"/>
        <source>Loading emails...</source>
        <translation>Loading emails...</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="167"/>
        <source>Email Accounts</source>
        <translation>Email Accounts</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="24"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="48"/>
        <source>Sort by Sender</source>
        <translation>Sort by Sender</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="75"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="109"/>
        <source>Set Password</source>
        <translation>Set Password</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="128"/>
        <source>Save to HTML</source>
        <translation>Save to HTML</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="155"/>
        <source>Changes take effect after restarting the app</source>
        <translation>Changes take effect after restarting the app</translation>
    </message>
</context>
</TS>
