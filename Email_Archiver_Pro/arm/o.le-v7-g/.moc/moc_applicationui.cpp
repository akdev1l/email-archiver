/****************************************************************************
** Meta object code from reading C++ file 'applicationui.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/applicationui.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'applicationui.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ApplicationUI[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,
      28,   14,   14,   14, 0x0a,
      58,   44,   14,   14, 0x0a,
     111,   90,   85,   14, 0x0a,
     148,  137,   14,   14, 0x0a,
     170,  137,   85,   14, 0x0a,
     195,   14,   14,   14, 0x0a,

 // methods: signature, parameters, type, tag, flags
     232,  221,   14,   14, 0x02,
     268,  257,   14,   14, 0x02,
     302,  292,   14,   14, 0x02,
     328,   14,   14,   14, 0x02,
     340,  292,   14,   14, 0x02,
     374,  365,   14,   14, 0x02,
     403,  365,   14,   14, 0x02,
     434,   14,   14,   14, 0x02,
     451,   14,   14,   14, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_ApplicationUI[] = {
    "ApplicationUI\0\0changeList()\0batteryChange()\0"
    "p_Key,p_Value\0saveValueFor(QString,bool)\0"
    "bool\0p_Key,p_DefaultValue\0"
    "getValueFor(QString,bool)\0p_Password\0"
    "savePassword(QString)\0comparePassword(QString)\0"
    "onSystemLanguageChanged()\0p_FilePath\0"
    "readEMLFile(QStringList)\0p_IsSender\0"
    "changeSortingKeys(bool)\0indexPath\0"
    "showMessage(QVariantList)\0getEmails()\0"
    "showEmails(QVariantList)\0filePath\0"
    "backUpAllEmails(QStringList)\0"
    "backUpSingleEmail(QStringList)\0"
    "setActiveFrame()\0accountNumber()\0"
};

void ApplicationUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ApplicationUI *_t = static_cast<ApplicationUI *>(_o);
        switch (_id) {
        case 0: _t->changeList(); break;
        case 1: _t->batteryChange(); break;
        case 2: _t->saveValueFor((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const bool(*)>(_a[2]))); break;
        case 3: { bool _r = _t->getValueFor((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: _t->savePassword((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: { bool _r = _t->comparePassword((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: _t->onSystemLanguageChanged(); break;
        case 7: _t->readEMLFile((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 8: _t->changeSortingKeys((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 9: _t->showMessage((*reinterpret_cast< QVariantList(*)>(_a[1]))); break;
        case 10: _t->getEmails(); break;
        case 11: _t->showEmails((*reinterpret_cast< QVariantList(*)>(_a[1]))); break;
        case 12: _t->backUpAllEmails((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 13: _t->backUpSingleEmail((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 14: _t->setActiveFrame(); break;
        case 15: _t->accountNumber(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ApplicationUI::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ApplicationUI::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ApplicationUI,
      qt_meta_data_ApplicationUI, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ApplicationUI::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ApplicationUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ApplicationUI::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ApplicationUI))
        return static_cast<void*>(const_cast< ApplicationUI*>(this));
    return QObject::qt_metacast(_clname);
}

int ApplicationUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
