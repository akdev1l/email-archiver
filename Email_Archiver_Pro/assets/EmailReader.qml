import bb.cascades 1.0
import bb.cascades.pickers 1.0

Page {
    titleBar: TitleBar {
        objectName: "timeStamp"
        title: qsTr("Email")+ Retranslate.onLocaleOrLanguageChanged
    }
    actions: [
        ActionItem {
            objectName: "backupOne"
            title: qsTr("Back up!")+ Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///icons/backup.png"
            onTriggered: {
                filePick.open();
            }
            ActionBar.placement: ActionBarPlacement.OnBar
        }
    ]
    Container {
        layout: DockLayout {

        }
        attachedObjects: [
            FilePicker {
                title: qsTr("Save as..")+ Retranslate.onLocaleOrLanguageChanged
                objectName: "filePick"
                id: filePick
                mode: FilePickerMode.Saver
                allowOverwrite: true
                onFileSelected: {
                    app.backUpSingleEmail(selectedFiles)
                }
            },
            Invocation {
                id: invokateBrowser
                query{
                    mimeType: "text/html"
                    onUriChanged: {
                        invokateBrowser.query.updateQuery()
                    }
                }
                onArmed: {
                    if(query.uri!=""){
                        trigger("bb.action.OPEN")
                    }
                }
            }
        ]
        Container {
            Container{
                id: infoContainer
                Label {
                    objectName: "subjectLabel"
                    text: ""
                    textStyle.fontSize: FontSize.Medium
                    multiline: true
                }
                Label {
                    objectName: "senderLabel"
                    textStyle.fontSize: FontSize.Small
                    textStyle.fontWeight: FontWeight.W100
    
                }
                Divider {
                }
            }
            ScrollView {
                id: scroll
                preferredHeight: 600
                scrollViewProperties.minContentScale: 1.0
                scrollViewProperties.maxContentScale: 4.0
                scrollViewProperties.scrollMode: ScrollMode.Both
                scrollViewProperties.pinchToZoomEnabled: true
                scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill
                WebView {
                    objectName: "bodyWebView"
                    settings.javaScriptEnabled: false
                    minWidth: 720.0
                    minHeight: 600.0
                    onLoadProgressChanged: {
                        progressBar.visible = true
                        progressBar.value = loadProgress
                        progressBar.toValue = loadProgress.MAX_VALUE
                        progressBar.fromValue = loadProgress.MIN_VALUE
                    }
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Succeeded) {
                            progressBar.visible = false
                        }
                    }
                    onNavigationRequested: {
                        console.log(settings.defaultTextCodecName)
                        if (request.url != "local:///") {
                            request.action = WebNavigationRequestAction.Ignore
                            invokateBrowser.query.uri = request.url
                        }
                    }
                    settings.defaultTextCodecName: "UTF-8"
                    settings.defaultFontSize: 100
                }
            }
        }
        ProgressIndicator {
            fromValue: 0.0
            toValue: 100.0
            id: progressBar
            preferredWidth: 768.0
            minWidth: 720.0
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom

        }
    }
}
