import bb.cascades 1.0

Container {
    topPadding: 3.0
    rightPadding: 3.0
    bottomPadding: 3.0
    leftPadding: 3.0
    layout: DockLayout {
    
    }
    background: Color.create("#ff484848")
    Container {
    topPadding: 1.0
    rightPadding: 1.0
    bottomPadding: 1.0
    leftPadding: 1.0
    layout: DockLayout {

    }
    background: Color.create("#ff8f8f8f")
    verticalAlignment: VerticalAlignment.Fill
    horizontalAlignment: HorizontalAlignment.Fill
    Container {
        verticalAlignment: VerticalAlignment.Fill
        
        layout: DockLayout {
        
        }
        background: Color.create("#ff3a3a3a")
            horizontalAlignment: HorizontalAlignment.Fill
        leftPadding: 10.0
        topPadding: 10.0
        rightPadding: 10.0
        bottomPadding: 10.0
        
        Container {
            layout: DockLayout {
            
            }
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            background: Color.Black
                Container {
                //        preferredHeight: 211.0
                horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Center
                Container {
                        layout: DockLayout {

                        }
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Fill
                        ImageView {
                            imageSource: "asset:///images/activeframe_bar.png"
                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Center

                        }
                        Container {
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Fill
//                            background: Color.create("#ffff9d9d")
                            
                            Label {
                                id: activeText
                                objectName: "activeText"
                                text: ""
                                textStyle.textAlign: TextAlign.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                onCreationCompleted: {
                                    Application.thumbnail.connect(onThumbnail)
                                }
                                textStyle.fontSize: FontSize.Large
                                textStyle.fontWeight: FontWeight.W200
                                textStyle.color: Color.White
                                verticalAlignment: VerticalAlignment.Center
                                function onThumbnail(){
                                    _Active.accountNumber();
                                    _Active.batteryChange();
                                }
                            }
                        }
                    }
                Container {
                        layout: DockLayout {

                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        
                        ImageView {
                            imageSource: "asset:///images/activeframe_bar.png"
                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Center

                        }
                        Container {
                            layout: DockLayout {

                            }
                            horizontalAlignment: HorizontalAlignment.Fill
                          
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                horizontalAlignment: HorizontalAlignment.Center
                                verticalAlignment: VerticalAlignment.Center
                                
                                ImageView {
                                    id: messagesicon
                                    scalingMethod: ScalingMethod.AspectFit
                                    preferredWidth: 65.0
                                    preferredHeight: 65.0
                                    verticalAlignment: VerticalAlignment.Center
                                    imageSource: "asset:///images/emails.png"
                                    visible: false
                                }
                                Label {
                                    id: messagesText
                                    objectName: "messagesText"
                                    text: ""
                                    onTextChanged: {
                                        messagesicon.visible = true;
                                    }
                                    textStyle.textAlign: TextAlign.Center
                                    horizontalAlignment: HorizontalAlignment.Center
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.W400
                                    textStyle.color: Color.White
                                    verticalAlignment: VerticalAlignment.Center
                                }
                            }
                        }
                    }
                Container {
                    layout: DockLayout  {
                    
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Bottom
                    topPadding: 20.0
                    Container {
                        layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight

                            }
                            horizontalAlignment: HorizontalAlignment.Center
                        
                        verticalAlignment: VerticalAlignment.Bottom
                     
                        Container {
                                verticalAlignment: VerticalAlignment.Center

                                ImageView {
                                imageSource: if (batteryText.text < 15) {
                                    "asset:///images/batt_low.png"
                                } else if (batteryText.text > 15 && batteryText.text < 25) {
                                    "asset:///images/batt_15.png"
                                } else if (batteryText.text >= 25 && batteryText.text <= 40) {
                                    "asset:///images/batt_25.png"
                                } else if (batteryText.text >= 40 && batteryText.text <= 60) {
                                    "asset:///images/batt_50.png"
                                } else if (batteryText.text >= 60 && batteryText.text <= 74) {
                                    "asset:///images/batt_70.png"
                                } else if (batteryText.text >= 75 && batteryText.text <= 95) {
                                    "asset:///images/batt_90.png"
                                } else if (batteryText.text > 95) {
                                    "asset:///images/batt_100.png"
                                }
                                scalingMethod: ScalingMethod.AspectFit
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 135.0
                                preferredHeight: 135.0
                                opacity: 0.6
                            }
                        }
                        Container {
                                layout: DockLayout {

                                }
                                verticalAlignment: VerticalAlignment.Center
                                Label {
                                
                                text: batteryText.text + "%"
                                textStyle.textAlign: TextAlign.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle.fontSize: FontSize.XLarge
                                textStyle.fontWeight: FontWeight.W700
                                verticalAlignment: VerticalAlignment.Center
                                textStyle.color: Color.Black
                            }            
                            Label {
                                
                                text: batteryText.text + "%"
                                textStyle.textAlign: TextAlign.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle.fontSize: FontSize.XLarge
                                textStyle.fontWeight: FontWeight.W400
                                verticalAlignment: VerticalAlignment.Center
                                textStyle.color: Color.White
                            } 
                        }
                        Label {
                            id: batteryText
                            objectName: "batteryText"
                            text: ""
                            textStyle.textAlign: TextAlign.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            textStyle.fontSize: FontSize.Large
                            textStyle.fontWeight: FontWeight.W500
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.color: Color.White
                            visible: false
                        } 
                        }
                
                }   
            }
        }
    }
}
}