APP_NAME = EmailArchiver
 
CONFIG += qt warn_on cascades10
LIBS += -lbbpim
LIBS += -lbbcascadespickers
LIBS += -lbbdevice
LIBS += -lbbplatformbbm -lbbsystem

include(config.pri)

TRANSLATIONS = \
    $${TARGET}_fr.ts \
    $${TARGET}_es.ts \
    $${TARGET}_en.ts \
    $${TARGET}.ts