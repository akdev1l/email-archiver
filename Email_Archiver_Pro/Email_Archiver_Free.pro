APP_NAME = Email_Archiver_Free

CONFIG += qt warn_on cascades10

include(config.pri)

TRANSLATIONS = \
    $${TARGET}_fr.ts \
    $${TARGET}_es.ts \
    $${TARGET}_en.ts \
    $${TARGET}.ts